BEGIN;

INSERT INTO usuario(
	senha, email, tipo_sanguineo, sexo, genero, login, cpf, dt_nascimento)
	VALUES
	('123456', 'joao@gmail.com', 'A+', 'homem', 'masculino', 'joaojoao', '994.860.760-06', '1980-10-10'),
  ('654321', 'pedro@gmail.com', 'A-', 'homem', 'masculino', 'pedro123', '421.010.620-89', '1981-11-16'),
  ('marcia123456', 'marcia@gmail.com', 'AB+', 'mulher', 'feminino', 'marcia49', '423.938.300-57', '1982-12-18'),
  ('98743igor', 'igor@gmail.com', 'O+', 'homem', 'masculino', 'igor321', '402.457.030-71', '1983-09-25'),
  ('iago123iago', 'iago@gmail.com', 'O+', 'homem', 'masculino', 'iagojc', '767.521.070-09', '1984-10-30'),
  ('435paulo', 'paulo@gmail.com', 'B-', 'homem', 'masculino', 'paulo435', '370.516.090-76', '1985-11-06'),
  ('carlos1508', 'carlos@gmail.com', 'B+', 'homem', 'masculino', 'carlos091', '107.971.590-88', '1986-08-15'),
  ('tiago1802', 'tiago@gmail.com', 'O-', 'homem', 'masculino', 'tiago142', '175.157.310-95', '1987-02-18'),
  ('999maria22', 'maria@gmail.com', 'O-', 'mulher', 'feminino', 'maria999', '610.921.160-52', '1988-03-22'),
  ('15ana08', 'ana@gmail.com', 'AB-', 'mulher', 'feminino', 'anasilva977', '871.763.500-43', '1989-08-15');


INSERT INTO torneio(
	valor_torneio, nome_torneio, status_torneio, descricao, qtd_vagas, aviso)
	VALUES
	('100.00', '1 torneio soccer', 'concluido', 'Esse é o primeiro torneio de futebol society do app', 10, 'O período de inscrições vai até o final de dezembro/2021'),
	('200.00', '2 torneio soccer', 'concluido', 'Esse é o segundo torneio de futebol society do app', 12, 'O período de inscrições vai até o final de janeiro/2022'),
	('300.00', '3 torneio soccer', 'concluido', 'Esse é o terceiro torneio de futebol society do app', 14, 'O período de inscrições vai até o final de fevereiro/2022'),
	('100.00', '4 torneio soccer', 'concluido', 'Esse é o quarto torneio de futebol society do app', 16, 'O período de inscrições vai até o final de março/2022'),
	('200.00', '5 torneio soccer', 'concluido', 'Esse é o quinto torneio de futebol society do app', 18, 'O período de inscrições vai até o final de abril/2022'),
	('300.00', '6 torneio soccer', 'aberto', 'Esse é o sexto torneio de futebol society do app', 20, 'O período de inscrições vai até o final de maio/2022'),
	('100.00', '7 torneio soccer', 'aberto', 'Esse é o sétimo torneio de futebol society do app', 10, 'O período de inscrições vai até o final de junho/2022'),
	('200.00', '8 torneio soccer', 'aberto', 'Esse é o oitavo torneio de futebol society do app', 12, 'O período de inscrições vai até o final de julho/2022'),
	('300.00', '9 torneio soccer', 'aberto', 'Esse é o nono torneio de futebol society do app', 14, 'O período de inscrições vai até o final de agosto/2022'),
	('100.00', '10 torneio soccer', 'aberto', 'Esse é o décimo torneio de futebol society do app', 16, 'O período de inscrições vai até o final de setembro/2022');


INSERT INTO ranking(
	descricao, qtd_ranking, nota, tempo, status_ranking, tipo)
	VALUES 
	('mais gols no ano', 90, 10, null, 'andamento', 'futebol'),
  ('mais veloz na subida', 30, 9, '00:30:00', 'andamento', 'bike'),
  ('pontos de ace no tenis', 10, 9, null, 'andamento', 'tenis'),
  ('mais veloz na descida', 30, 9, '00:10:00', 'concluido', 'bike'),
  ('mais veloz na escalada', 16, 10, '01:30:00', 'andamento', 'escalada'),
  ('mais pontos de ace no volei', 50, 10, null, 'concluido', 'volei'),
  ('levanta mais peso', 12, 8, null, 'andamento', 'peso'),
  ('nocaute mais rapido box', 20, 8, '00:00:30', 'andamento', 'boxe'),
  ('ippon mais rapido judo', 20, 8, '00:00:15', 'concluido', 'judo'),
  ('mais manobras dificeis', 40, 10, null, 'andamento', 'manobras');


insert into banco (banco,agencia,conta,operacao)
values ('BRADESCO',1771,123456,0),
		('BRADESCO',1771,654252,0),
        ('CAIXA',2193,123456,13),
        ('CAIXA',1123,25847,21),
        ('BRASIL',1235,789452,0),
        ('BRASIL',1235,965878,0),
		('NUBANK',170,87587,0),
        ('NUBANK',171,547852,0),
        ('ITAU',1145,478547,0),
        ('SANTANDER',1717,254878,0);
		
		
insert into caixa (data,status_caixa,saldo,entradas,saidas)
values  ('2022-05-25','ANALISANDO', 100.00 , 10.00 , 20.00),
		('2022-05-20','APROVADO', 200.00,20.00,0.00),
        ('2022-05-24','PENDENTE',0.00,100.00,0.00),
        ('2022-05-25','ANALISANDO',20.00,0.00,20.00),
		('2022-05-01','PENDENTE',100.00,0.00,20.00),
        ('2022-05-20','APROVADO',800.00,0.00,20.00),
        ('2022-05-15','ANALISANDO',900.00,30.00,50.00),
        ('2022-05-12','PENDENTE',300.00,20.00,40.00),
        ('2022-05-23','APROVADO',200.00,100.00,200.00),
        ('2022-05-22','ANALISANDO',700.00,100.00,30.00);
		
INSERT into categoria (descricao)
values ('FUTEBOL'),
	   ('VOLEI'),
       ('FUTSAL'),
       ('FUTVOLEI'),
       ('FUTSAL'),
       ('TENIS'),
       ('NATAÇÃO'),
       ('BASQUETE'),
       ('PAINTBALL'),
       ('TENIS DE MESA');

INSERT INTO CONTATO (telefone, rede_social, nome, celular, email, whatsapp)
VALUES ('(81)1111-1111','@joao','Joao','(81)1111-2222','joao@gmail.com','(81)1111-2222'),
	   ('(81)2222-2222','@maria','Maria','(81)2222-3333','maria@gmail.com','(81)2222-3333'),
	   ('(81)3333-3333','@matheus','matheus','(81)3333-2222','matheus@gmail.com','(81)3333-2222'),
		('(81)4444-1111','@francisco','Francisco','(81)1111-3333','francisco@gmail.com','(81)1111-3333'),
		('(81)1111-3333','@jose','Jose','(81)6666-2222','jose@gmail.com','(81)6666-2222'),
		('(81)6666-1111','@lucas','lucas','(81)8888-2222','lucas@gmail.com','(81)8888-2222'),
		('(81)9999-1111','@matias','matias','(81)8888-3333','matias@gmail.com','(81)8888-3333'),
		('(81)4444-2211','@zed','Zed','(81)3333-8752','zed@gmail.com','(81)3333-8752'),
		('(81)5555-1111','@tobias','tobias','(81)5588-2222','tobias@gmail.com','(81)5588-2222'),
		('(81)1111-9999','@paul','Paul','(81)1111-9988','paul@gmail.com','(81)1111-9988');
		

INSERT into evento (nome_evento, descricao, valor_evento, qtd_vagas, aviso, restricao, status_evento)
VALUES ('Futlindo', 'Futebol Lindo',10.00, 26,'Cheguem no horario','18 anos +','Aberto'),
		('VoleiLindo', 'volei Lindo',10.00, 16,'Cheguem no horario','18 anos +','Aberto'),
        ('TenisMaroto', 'Tenis Maroto',50.00, 2,'Cheguem no horario','18 anos +','Fechado'),
        ('Fut do Sasa', 'Futevolei do sasa',5.00, 10,'Cheguem no horario','Não há','Aberto'),
        ('Cesta Certa', 'Basquete do bom',10.00, 20,'Tragam agua','18 anos +','Aberto'),
        ('Futsal do gogo', 'Fut do vai vai',5.00, 20,'Cheguem no horario','Não há','Aberto'),
        ('Mesa open', 'Tenis de Mesa livre',10.00, 26,'Cheguem no horario','Não há','Aberto'),
        ('FutArt', 'Futebol Arte',10.00, 26,'Cheguem no horario','Não há','Aberto'),
        ('Aqua Livre', 'Natação para todos',10.00, 8,'Cheguem no horario','Não há','Aberto'),
        ('Volei LGBT', 'Volei Livre',10.00, 20,'Cheguem no horario','Não há','Aberto');
		
		
INSERT INTO endereco(id_endereco, rua, bairro, cidade, uf, cep, numero, complemento)
	VALUES ('Rua Chivo' , 'Bongi' , 'Recife' , 'PE' , '50089-020' , '90' , 'casa'),
	       ('Rua das Flores' , 'Prado' , 'Recife' , 'PE' , '59089-920' , '89' , 'apt 101'),
	       ('Rua Estevao' , 'Torre' , 'Recife' , 'PE' , '55089-090' , '105' , 'casa'),
	       ('Av Dantas' , 'Centro' , 'Recife' , 'PE' , '10089-920' , '25' , 'casa'),
	       ('Rua 5 Pontas' , 'Rio Doce' , 'Olinda' , 'PE' , '50089-120' , '89' , 'apt 202'),
	       ('Rua Nestor' , 'Espinheiro' , 'Recife' , 'PE' , '60089-080' , '90' , 'apt 403'),
	       ('Rua Dr.Carlos' , 'St.Amaro' , 'Recife' , 'PE' , '50089-030' , '102' , 'casa'),
	       ('Rua Azulita' , 'Casa Caiada' , 'Olinda' , 'PE' , '40089-020' , '33' , 'casa'),
	       ('Rua da Pedra' , 'Varadouro' , 'Olinda' , 'PE' , '14589-000' , '44' , 'casa'),
	       ('Rua da Madeira' , 'Afogados' , 'Recife' , 'PE' , '58989-020' , '55' , 'apt 104');

INSERT INTO pagamento(
	valor_pagamento, mes, nome, dia, ano, tipo_pagamento)
	VALUES
	     ('20.00','2021-01-20','pelada da sexta','2021-01-20','2021-01-20','mensal'),
	     ('20.00','2021-01-15','pelada da sexta','2021-01-15','2021-01-15','mensal'),
	     ('20.00','2021-01-21','pelada da sexta','2021-01-21','2021-01-21','mensal'),
	     ('60.00','2021-03-05','carro apoio ciclismo','2021-03-05','2021-03-05','trimestral'),
	     ('60.00','2021-03-15','carro apoio ciclismo','2021-03-15','2021-03-15','trimestral'),
	     ('60.00','2021-03-25','carro apoio ciclismo','2021-03-25','2021-03-25','trimestral'),
	     ('120.00','2021-09-20','XX meia maratona','2021-09-20','2021-09-20','inscricao'),
	     ('120.00','2021-09-12','XX meia maratona','2021-09-12','2021-09-12','inscricao'),
	     ('120.00','2021-09-17','XX meia maratona','2021-09-17','2021-09-17','inscricao'),
	     ('50.00','2021-10-13','tenis das quartas','2021-10-13','2021-10-13','horaquadra');

INSERT INTO fidelidade(Tipo_Fidelidade, Descricao, Inicio, Fim)
VALUES ('Temporaria','Prazo 30 dias', '2022-01-01' , '2022-01-30'),
		('Temporaria','Prazo 60 dias','2022-01-01', '2022-03-01'),
        ('Temporaria','Prazo 15 dias','2022-01-01', '2022-01-15'),
        ('Vitalicia','Prazo Intederminado','2022-02-01','2050-01-01'),
        ('Promocional','Prazo Intederminado','2022-01-01','2050-01-01'),
        ('Promocional','Prazo Definido','2022-01-20','2022-03-20'),
        ('Recompensas','Prazo Definido','2022-03-01', '2022-03-15'),
        ('Recompensas','Prazo Intederminado','2022-01-01','2050-01-01'),
        ('Parceria','Prazo Intederminado','2022-01-15','2050-01-01'),
        ('Parceria','Prazo Definido','2022-01-01','2022-12-31');

INSERT into periodo(status_periodo, data_final, data_inicio,hora_inicio,hora_fim,id_usuario)
VALUES ('ABERTO','2022-05-31', '2022-05-31','19:00:00','21:00:00',1),
		('ABERTO','2022-04-30', '2022-04-30','20:00:00','21:00:00',2),
        ('FECHADO','2022-05-31', '2022-05-31','15:00:00','20:00:00',3),
        ('FECHADO','2022-05-03', '2022-05-03','13:00:00','22:00:00',4),
        ('CANCELADO','2022-07-31', '2022-07-31','19:00:00','21:00:00',5),
        ('CANCELADO','2022-06-01', '2022-06-01','19:00:00','21:00:00',6),
        ('REABERTO','2022-05-15', '2022-05-31','20:00:00','23:00:00',7),
        ('REABERTO','2022-05-20', '2022-06-30','18:00:00','21:00:00',8),
        ('INDEFINIDO','2022-06-15', '2022-06-15','18:00:00','22:00:00',9),
        ('INDEFINIDO','2022-07-30', '2022-07-30','19:00:00','23:00:00',10);

INSERT INTO grupo(
	nome_grupo, aviso, fila_espera, status_grupo)
	VALUES
      ('pelada da sexta','pelada mais animada da sexta-feira','fila1','aberto'),
      ('pelada da quarta','pelada mais amada da quarta-feira','fila2','aberto'),
      ('pelada dos amigos','aqui só tem os brothers','fila3','fechado'),
      ('trilha do ciclismo','só os fortes e aventureiros','fila4','fechado'),
      ('bikes do asfalto','velocidade é o que nos move','fila5','indefinido'),
      ('lama na bike','se tem lama, lá estaremos','fila6','indefinido'),
      ('perebas do tenis','venha fazer parte dessa família dos que não sabem jogar tenis','fila7','aberto'),
      ('street racers','grupo certo para quem gosta de correr na cidade','fila8','aberto'),
      ('maratonar','aqui só tem os resistentes na corrida','fila9','fechado'),
      ('king ping pong','para quem sabe e também não sabe o tênis de mesa','fila10','aberto');

INSERT INTO premiacao(
	tipo_premiacao, custo_premiacao, status_premiacao, qtd_premiacao)
	VALUES
	     ('primeiro colocado',100,'pago',1),
	     ('segundo colocado',80,'pago',1),
	     ('terceiro colocado',50,'pago',1),
	     ('melhor tempo',200,'pendente',1),
	     ('melhores tempos',100,'pendente',5),
	     ('goleadores',70,'pago',3),
	     ('mais pontos',250,'pago',3),
	     ('mais veloz',300,'pendente',1),
	     ('melhor manobra',50,'pago',1),
	     ('pior manobra',30,'pago',1);

COMMIT;